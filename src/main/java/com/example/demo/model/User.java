package com.example.demo.model;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	private int id;

	@Column(name="username",unique= true)
	private String username;
	
	@Column(name="email", nullable = true)
	private String email;
	
	@Column(name="password")
	private String password;
	public User() {
    }
	public User(int id) {
		this.id=id;
    }
	public User(Department dep) {
		this.department=dep;
	}
	
	public User(User user) {
		this.id = user.getId();
		this.email = user.getEmail();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.deleted = user.isDeleted();
		this.name = user.getName();
		this.surname =user.getSurname();
        this.role = user.getRole();
        this.created_by = user.getCreated_by();
        this.updated_by = user.getUpdated_by();
        this.created_at = user.getCreated_at();
        this.updated_at = user.getUpdated_at();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="name", nullable = true)
	private String name;
	
	@Column(name="surname", nullable = true)
	private String surname;
	
	@Column(name="deleted", nullable = true)
	private boolean deleted;
	
	@Column(name="role", nullable = true)
	private String role;
	
	@Column(name="created_by", nullable = true)
	private int created_by;
	
	@Column(name="updated_by", nullable = true)
	private int updated_by;
	
	@Column(name="created_at", nullable = true)
	private Date created_at;
	
	@Column(name="updated_at", nullable = true)
	private Date updated_at;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "user_department_id")
	private Department department;
	
	
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getCreated_by() {
		return created_by;
	}

	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}

	public int getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(int update_by) {
		this.updated_by = update_by;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	
	
}
