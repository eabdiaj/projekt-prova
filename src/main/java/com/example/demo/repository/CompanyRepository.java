package com.example.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.model.Company;

public interface CompanyRepository extends PagingAndSortingRepository<Company,Integer>{
//	public List<Company> getCompaniesByDeleted(boolean deleted);
	
	public Page<Company> getCompaniesByDeleted(boolean deleted, Pageable page);
}
