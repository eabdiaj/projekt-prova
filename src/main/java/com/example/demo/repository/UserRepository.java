package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	public List<User> getUsersByDepartmentId(int id);
	
	public List<User> getUsersByDepartmentCompanyId(int id);
	
	public List<User> getUsersByDeleted(boolean deleted);
	
	Optional<User> findByUsername(String username);
}
