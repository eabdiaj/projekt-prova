package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Department;


public interface DepartmentRepository extends CrudRepository<Department,Integer>{
	public List<Department> getDepartmentsByCompanyId(int id);
	
	
	public List<Department> getDepartmentsByDeleted(boolean deleted);
}
