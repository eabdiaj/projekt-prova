package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Document;
import com.example.demo.model.User;

public interface DocumentRepository extends CrudRepository<Document,Integer>{
	public List<Document> getDocumentsByDeleted(boolean deleted);
	public List<Document> getDocumentsByOwnerId(int id);
}
