package com.example.demo.controller;

import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.service.CompanyService;
import com.example.demo.service.DocumentService;
import com.example.demo.service.UserService;

@PreAuthorize("hasAnyRole('MANAGER')")
@RequestMapping("/dashboard")
@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DocumentService documentService;
	
	@GetMapping("/users")
	public ModelAndView allUsers(ModelMap model){
		return new ModelAndView("Users/index","users",userService.getAllUser());
	}
	
	@GetMapping("/users/{id}/documents")
	public ModelAndView allDocumentsByUser(@PathVariable int id,ModelMap model){
		return new ModelAndView("Documents/documents","documents",documentService.getAllDocumentsByUser(id));
	}
	
	@GetMapping("/users/{id}/edit")
	public ModelAndView getUser(@PathVariable int id,ModelMap model){
		return new ModelAndView("Users/editUser","user",userService.getUser(id)); 
	}
	
	@GetMapping("/departments/{id}/users/add")
	public ModelAndView getAddUser(Model model,@PathVariable int id){
		return new ModelAndView("Users/addUser","user",new User(new Department(id)));
	}
	
	@PostMapping("/users")
	public String addUser(@ModelAttribute User user, BindingResult bindingResult,ModelMap model)
	{
		if(bindingResult.hasErrors()){
			System.out.println("There was a error "+bindingResult);
            return "redirect:/dashboard/users";
		}
		userService.addUser(user);
		return "redirect:/dashboard/users";
	}
	
	@PutMapping("/users/{id}")
	public String updateUser(@ModelAttribute User user,BindingResult bindingResult,@PathVariable int id) {
		if(bindingResult.hasErrors()){
			System.out.println("There was a error "+bindingResult);
            return "redirect:/dashboard/users";
		}
		userService.updateUser(id, user);
		return "redirect:/dashboard/users";
	}
	
	@DeleteMapping("/users/{id}")
	public String deleteUser(@PathVariable int id) {
		userService.deleteUser(id);
		return "redirect:/dashboard/users";
	}
	
}
