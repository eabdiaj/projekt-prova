package com.example.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.model.Company;
import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.service.CompanyService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/dashboard")
@Controller
public class CompanyController {
	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@GetMapping("/companies")
	public String allCompanies(){
//		return new ModelAndView("Companies/companies","companies",companyService.getAllCompanies());
		return "Companies/companies";
	}

	@GetMapping("/companies/{cid}/users")
	public ModelAndView allUsersByComp(@PathVariable int cid,ModelMap model){
		return new ModelAndView("Users/index","users",userService.getAllUserByCompany(cid));
	}
	
	@GetMapping("/companies/add")
	public ModelAndView getAddCompany(Model model){
		return new ModelAndView("Companies/addCompany","company",new Company());
	}
	
	@GetMapping("/companies/{id}/edit")
	public ModelAndView getCompanyEdit(Model model,@PathVariable int id){
		return new ModelAndView("Companies/editCompany","company",companyService.getCompany(id));
	}
	
	@GetMapping("/companies/{id}/departments")
	public ModelAndView getDepartmentsByCompany(ModelMap model,@PathVariable int id){
		return new ModelAndView("Departments/departments","departments",companyService.getCompanysDep(id));
	}
	
	@PutMapping("/companies/{id}")
	public String updateCompany(@ModelAttribute Company company, BindingResult bindingResult,@PathVariable int id,ModelMap model) {
		//check for errors
		if(bindingResult.hasErrors()){
			System.out.println("There was a error "+bindingResult);
            return "redirect:/dashboard/companies";
		}
		companyService.updateCompany(company,id);
		return "redirect:/dashboard/companies";
	}
	
	@PostMapping("/companies")
	public String addCompany(@ModelAttribute Company company, BindingResult bindingResult,ModelMap model)
	{
		//check for errors
		if(bindingResult.hasErrors()){
			System.out.println("There was a error "+bindingResult);
            return "redirect:/dashboard/companies";
		}
		companyService.addCompany(company);
		return "redirect:/dashboard/companies";
	}
	
	@DeleteMapping("/companies/{id}")
	public String deleteCompany(@PathVariable int id) {
		companyService.deleteCompany(id);
		return "redirect:/dashboard/companies";
	}
	
	
	@PostMapping("/companies/{id}/administrator")
	public String setDepartmentManager(@PathVariable int id,@RequestParam("user") int uid) {
		Company comp = companyService.getCompany(id);
		User administrator = comp.getAdministrator();
		User nadministrator = userService.getUser(uid);
		if(administrator!=null)
			userService.updateUserRole("ROLE_USER",administrator);
		
	    userService.updateUserRole("ROLE_ADMINISTRATOR",nadministrator);
	    companyService.updateCompanyAdmin(comp,nadministrator);
		return "redirect:/dashboard/companies";
	}
	
	

}
