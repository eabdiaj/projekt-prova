package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Company;
import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@PreAuthorize("hasAnyRole('ADMINISTRATOR')")
@RequestMapping("/dashboard")
@Controller
public class DepartmentController {
	@Autowired
	private DepartmentService departService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/departments")
	public String allDepartments(ModelMap model){
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		cuser=userService.getUser(cuser.getId());
		List<Department> departments = departService.getAllDepartment();
		model.addAttribute("departments", departments);
		model.addAttribute("cuser",cuser);
		return "Departments/departments";
	}

	@GetMapping("/departments/{id}/users")
	public ModelAndView allUsersByDep(@PathVariable int id ,ModelMap model){
		return new ModelAndView("Users/index","users",userService.getAllUserByDep(id));
	}
	
	@GetMapping("/companies/{id}/departments/add")
	public ModelAndView getAddDepartment(Model model,@PathVariable int id){
		return new ModelAndView("Departments/addDepartment","department",new Department(new Company(id)));
	}
	
	@PostMapping("/departments")
	public String addDepartment(@ModelAttribute Department department, BindingResult bindingResult,ModelMap model)
	{
		//check for errors
		if(bindingResult.hasErrors()){
			System.out.println("There was a error "+bindingResult);
            return "redirect:/dashboard/departments";
		}
		departService.addDepartment(department);
		return "redirect:/dashboard/departments";
	}
	
	@GetMapping("/departments/{id}/edit")
	public ModelAndView getDepartmentEdit(Model model,@PathVariable int id){
		return new ModelAndView("Departments/editDepartment","department",departService.getDepartment(id));
	}
	
	@PutMapping("/departments/{id}")
	public String updateDepartment(@ModelAttribute Department department, BindingResult bindingResult,@PathVariable int id,ModelMap model) {
		if(bindingResult.hasErrors()){
			System.out.println("There was a error "+bindingResult);
            return "redirect:/dashboard/departments";
		}
		departService.updateDepartment(department,id);
		return "redirect:/dashboard/departments";
	}
	
	@DeleteMapping("/departments/{id}")
	public String deleteDepartment(@PathVariable int id) {
		departService.deleteDepartment(id);
		return "redirect:/dashboard/departments";
	}
	
	@PostMapping("/departments/{id}/manager")
	public String setDepartmentManager(@PathVariable int id,@RequestParam("user") int uid) {
		Department dep = departService.getDepartment(id);
		User manager = dep.getManager();
		User nmanager = userService.getUser(uid);
		if(manager!=null)
			userService.updateUserRole("ROLE_USER",manager);
		
	    userService.updateUserRole("ROLE_MANAGER",nmanager);
		departService.updateDepManager(dep,nmanager);
		return "redirect:/dashboard/departments";
	}
}
