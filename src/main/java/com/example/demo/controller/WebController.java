package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Controller
public class WebController {
	
	@RequestMapping(value={"/login"})
	public String login(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		    return "redirect:/dashboard/users";
		}
		return "login";
	}
	
	@RequestMapping(value={"/"})
	public String home(){
		return "redirect:/dashboard/users";
	}
	
	@RequestMapping(value={"/home"})
	public String home1(){
		return "index";
	}
}
