package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Company;
import com.example.demo.service.CompanyService;
import com.example.demo.service.UserService;

@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/api")
@RestController
public class CompanyApiController {
	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@GetMapping("/companies")
	public Page<Company> allCompanies(ModelMap model,@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page){
		if(pageSize.orElse(0) != 0)
		{
			System.out.println(pageSize.get()+ page.get());
			return companyService.getAllCompaniesPagable(new PageRequest(page.get(),pageSize.get()));
		}
		return companyService.getAllCompaniesPagable(new PageRequest(0,10));
	}
	
	@GetMapping("companies/{id}")
	public Company getCompany(@PathVariable int id)
	{
		return companyService.getCompany(id);
	}
	
	@PostMapping("/companies")
	public String addCompany(@RequestBody Company company)
	{
		companyService.addCompany(company);
		return "OK";
	}
	
	@PutMapping("/companies/{id}")
	public String updateCompany(@RequestBody Company company,@PathVariable int id) {
		companyService.updateCompany(company,id);
		return "OK";
	}
	
	@DeleteMapping("/companies/{id}")
	public void deleteCompany(@PathVariable int id) {
		System.out.println("bla");
		companyService.deleteCompany(id);
	}
}
