package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Document;
import com.example.demo.model.User;
import com.example.demo.service.DocumentService;;

@PreAuthorize("hasAnyRole('USER')")
@RequestMapping("/dashboard")
@Controller
public class DocumentController {
	@Autowired
	private DocumentService documentService;
	
	@GetMapping("/documents")
	public ModelAndView allDocuments(ModelMap model){
		return new ModelAndView("Documents/documents","documents",documentService.getAllDocuments());
	}
	
	@GetMapping("/documents/add")
	public String getAddDocuments(ModelMap model){
		return "Documents/addDocument";
	}
	
	@PostMapping("/documents")
	public @ResponseBody String addDocument(@RequestParam("file") MultipartFile file,ModelMap model)
	{
		Document d=  new Document();
		d.setUrl(documentService.store(file));
		d.setName(file.getOriginalFilename());
		documentService.addDocument(d);
		return "Succesful upload";
	}
	
	@DeleteMapping("/documents/{id}")
	public String deleteUser(@PathVariable int id) {
		documentService.deleteDocument(id);
		return "redirect:/dashboard/documents";
	}
	
	@GetMapping("/documents/{id}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable int id) {
		Resource file = documentService.loadFile(id);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	
}
