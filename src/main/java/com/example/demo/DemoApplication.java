package com.example.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;

import com.example.demo.service.DocumentService;

@SpringBootApplication
@EnableJpaRepositories("com.example.demo.repository")
@EnableConfigurationProperties(SecurityPropertiesExtension.class)
public class DemoApplication {
	
	public static void main(String[] args){
		RoleHierarchy roleHierarchy = SpringApplication.run(DemoApplication.class, args).getBean(RoleHierarchy.class);
	}
	
	@Bean
	RoleHierarchy roleHierarchy(SecurityPropertiesExtension spe) {
		return RoleHierarchyUtils.roleHierarchyFromMap(spe.getHierarchy());
	}
}



@ConfigurationProperties("security.roles")
class SecurityPropertiesExtension {

	Map<String, List<String>> hierarchy = new LinkedHashMap<>();

	public Map<String, List<String>> getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(Map<String, List<String>> hierarchy) {
		this.hierarchy = hierarchy;
	}
}

class RoleHierarchyUtils {

	public static RoleHierarchy roleHierarchyFromMap(Map<String, List<String>> roleHierarchyMapping) {

		StringWriter roleHierachyDescriptionBuffer = new StringWriter();
		PrintWriter roleHierarchyDescriptionWriter = new PrintWriter(roleHierachyDescriptionBuffer);

		for (Map.Entry<String, List<String>> entry : roleHierarchyMapping.entrySet()) {

			String currentRole = entry.getKey();
			List<String> impliedRoles = entry.getValue();

			for (String impliedRole : impliedRoles) {
				String roleMapping = currentRole + " > " + impliedRole;
				roleHierarchyDescriptionWriter.println(roleMapping);
			}
		}

		RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
		roleHierarchy.setHierarchy(roleHierachyDescriptionBuffer.toString());
		return roleHierarchy;
	}
}
