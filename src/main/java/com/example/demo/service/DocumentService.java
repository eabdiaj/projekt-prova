package com.example.demo.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Document;
import com.example.demo.model.User;
import com.example.demo.repository.DocumentRepository;
import java.util.Random;

@Service
public class DocumentService {
	@Autowired
	private DocumentRepository dDocumentocumentRepo;
	
	private final Path rootLocation = Paths.get("upload-dir");
	
	public String store(MultipartFile file){
		try {
			Random rand = new Random();
			String name=file.getOriginalFilename()+rand.nextInt(10000000) + 1;
			name = md5(name);
            Files.copy(file.getInputStream(), this.rootLocation.resolve(name));
            return name;
        } catch (Exception e) {
        	throw new RuntimeException("FAIL");
        }
	}
	
	public String md5(String name) throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] arr = md.digest(name.getBytes());
		return Base64.getEncoder().encodeToString(arr);
	}
	
	public List<Document> getAllDocuments(){
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Document> documents = new ArrayList<>();
		dDocumentocumentRepo.getDocumentsByOwnerId(cuser.getId())
				.forEach(documents::add);
	    return documents;
	}
	
	public List<Document> getAllDocumentsByUser(int id){
		List<Document> documents = new ArrayList<>();
		dDocumentocumentRepo.getDocumentsByOwnerId(id)
				.forEach(documents::add);
	    return documents;
	}
	
	public void addDocument(Document document) {
//		//set created by 
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		document.setOwner(cuser);
		document.setCreated_at(new Date());
		dDocumentocumentRepo.save(document);
	}
	
	public void deleteDocument(int id)
	{
		Document document = dDocumentocumentRepo.findOne(id);
		document.setDeleted(true);
		dDocumentocumentRepo.save(document);
	}
	
	public Resource loadFile(int id) {
        try {
        	String filename = dDocumentocumentRepo.findOne(id).getUrl();
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }else{
            	throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
        	throw new RuntimeException("FAIL!");
        }
    }
	
}
