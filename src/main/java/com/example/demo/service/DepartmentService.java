package com.example.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.repository.DepartmentRepository;


@Service
public class DepartmentService {
	
	@Autowired
	private DepartmentRepository departmentRepo;
	
	public List<Department> getAllDepartment(){
		List<Department> departments = new ArrayList<>();
		 
		departmentRepo.getDepartmentsByDeleted(false)
				.forEach(departments::add);
	    return departments;
	}
	
	public List<Department> getAllDepartmentByCompany(int comp_id){
		List<Department> departments = new ArrayList<>();
		 
		departmentRepo.getDepartmentsByCompanyId(comp_id)
				.forEach(departments::add);
	    return departments;
	}
	
	public Department getDepartment(int id) {
		return departmentRepo.findOne(id);
	}
	
	public void updateDepManager(Department dep,User manager) {
		dep.setManager(manager);
		departmentRepo.save(dep);
	}
	
	public void addDepartment(Department department) {

		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		//set created attributes
		department.setCreated_by(cuser.getId());
		department.setCreated_at(new Date());
		
		departmentRepo.save(department);
	}
	
	public void updateDepartment(Department department,int id) {
		Department current_dept = departmentRepo.findOne(id);
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		//update updated attributes
		current_dept.setUpdated_by(cuser.getId());
		current_dept.setUpdated_at(new Date());
		//update only form fields
		current_dept.setName(department.getName());
		current_dept.setDescription(department.getDescription());
		
		departmentRepo.save(current_dept);
	}
	
	public void deleteDepartment(int id)
	{
		//don't delete...update deleted
		Department department = departmentRepo.findOne(id);
		department.setDeleted(true);
		departmentRepo.save(department);
	}
	
	public Set<User> getDepUsers(int id)
	{
		Department department = departmentRepo.findOne(id);
		return department.getUsers();
	}
	
}
