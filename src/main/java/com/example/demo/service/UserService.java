package com.example.demo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepo;
	
	
	
	public List<User> getAllUser(){
		List<User> users = new ArrayList<>();
		//get users based on currently logged in user
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		cuser = userRepo.findOne(cuser.getId());
		switch(cuser.getRole()) {
			case "ROLE_ADMIN":
				userRepo.getUsersByDeleted(false)
				.forEach(users::add);
				break;
			case "ROLE_ADMINISTRATOR":
				System.out.println(cuser.getDepartment().getCompany().getId());
				users = this.getAllUserByCompany(cuser.getDepartment().getCompany().getId());
				break;
			case "ROLE_MANAGER":
				System.out.println(cuser.toString());
				users = this.getAllUserByDep(cuser.getDepartment().getId());
				break;
		}
	    return users;
	}
	
	public List<User> getAllUserByDep(int dep_id){
		List<User> users = new ArrayList<>();
		List<String> roles= Arrays.asList (
				"ROLE_USER","ROLE_MANAGER"
			);
	    userRepo.getUsersByDepartmentId(dep_id)
	    		.stream().filter(u -> roles.indexOf(u.getRole())!=-1).collect(Collectors.toList())
				.forEach(users::add);
	    return users;
	}
	
	public List<User> getAllUserByCompany(int comp_id){
		List<User> users = new ArrayList<>();
		List<String> roles= Arrays.asList (
			"ROLE_USER","ROLE_MANAGER","ROLE_ADMINISTRATOR"
		);
	    userRepo.getUsersByDepartmentCompanyId(comp_id)
	    		.stream().filter(u -> roles.indexOf(u.getRole())!=-1).collect(Collectors.toList())
				.forEach(users::add);
	    return users;
	}
	
	public User getUser(int id) {
		return userRepo.findOne(id);
	}
	
	public void addUser(User user) {
		
		//set created by 
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user.setCreated_by(cuser.getId());
		user.setCreated_at(new Date());
		user.setRole("ROLE_USER");
		
		userRepo.save(user);
	}
	
	public void updateUser(int id,User user) {
		User cuser= userRepo.findOne(id);

		User cuser1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		cuser.setUsername(user.getUsername());
		cuser.setName(user.getName());
		cuser.setSurname(user.getSurname());
		cuser.setEmail(user.getEmail());
		cuser.setUpdated_at(new Date());
		cuser.setUpdated_by(cuser1.getId());
		
		String password= user.getPassword();
		if(password !="" && password != cuser.getPassword())
			cuser.setPassword(password);
		userRepo.save(cuser);
	}
	
	public void updateUserRole(String role,User user) {
		user.setRole(role);
		userRepo.save(user);
	}
	
	public void deleteUser(int id)
	{
		User user = userRepo.findOne(id);
		user.setDeleted(true);
		userRepo.save(user);
	}
}
