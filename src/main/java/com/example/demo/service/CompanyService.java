package com.example.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.example.demo.model.Company;
import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.repository.CompanyRepository;

@Service
public class CompanyService {
	@Autowired
	private CompanyRepository companyRepo;
	
//	public List<Company> getAllCompanies(){
//		List<Company> companies = new ArrayList<>();
//		 
//		companyRepo.getCompaniesByDeleted(false)
//				.forEach(companies::add);
//		
//	    return companies;
//		
//	}
	
	
	public Page<Company> getAllCompaniesPagable(Pageable pageable){
//		List<Company> companies = new ArrayList<>();
//		 
//		companyRepo.getCompaniesByDeleted(false)
//				.forEach(companies::add);
//		
//	    return companies;
		return companyRepo.getCompaniesByDeleted(false, pageable);
	}
	
	public void addCompany(Company company) {
		
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		//set created attributes
		company.setCreated_by(cuser.getId());
		company.setCreated_at(new Date());
		
		companyRepo.save(company);
	}
	
	public Company getCompany(int id) {
		return companyRepo.findOne(id);
	}
	
	public Set<Department> getCompanysDep(int id)
	{
		return companyRepo.findOne(id).getDepartments();
	}
	
	public void deleteCompany(int id)
	{
		//don't delete...update deleted
		Company company = companyRepo.findOne(id);
		System.out.println("bla");
		company.setDeleted(true);
		System.out.println(company.isDeleted());
		companyRepo.save(company);
	}
	
	public void updateCompany(Company company,int id) {
		User cuser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Company currentCompany = companyRepo.findOne(id);
		//update only form fields
		currentCompany.setName(company.getName());
		currentCompany.setDescription(company.getDescription());
		
		//update updated attributes
		currentCompany.setUpdated_by(cuser.getId());
		currentCompany.setUpdated_at(new Date());
		
		companyRepo.save(currentCompany);
	}
	
	public void updateCompanyAdmin(Company company,User admin) {
		company.setAdministrator(admin);;
		companyRepo.save(company);
	}
	
}
