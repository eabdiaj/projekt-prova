-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 26, 2017 at 12:35 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 5.6.31-6+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projektProva`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `administrator` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `name`, `description`, `administrator`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'kompania test', 'kompania test112345', 2, 0, 1, '2017-10-23 22:00:00', '2017-10-24 10:13:19', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `department_company_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `name`, `description`, `manager`, `department_company_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'dep test', 'kjo ishte test1', 9, 1, 0, 1, '2017-10-23 22:00:00', '2017-10-24 10:15:13', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `document_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `owner` int(11) NOT NULL,
  `createD_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` bit(1) DEFAULT b'0',
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`document_id`, `name`, `owner`, `createD_at`, `deleted`, `url`) VALUES
(3, 'anketimi online 1.doc', 1, '2017-10-25 13:56:40', b'1', 'h0thRIDXB3cCAhD1xjwWjQ=='),
(4, 'CV-Europass-20171003-Abdiaj-EN  (3).pdf', 1, '2017-10-25 13:58:55', b'0', 'TwEzEc5mVrFiUyZYgtKjKQ=='),
(5, 'anketimi online 1.doc', 10, '2017-10-25 14:11:03', b'0', '64WVTuj54kVeqjzvIIpk5g==');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT 'ROLE_USER',
  `deleted` bit(1) DEFAULT b'0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `user_department_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `name`, `surname`, `role`, `deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`, `user_department_id`) VALUES
(1, 'admin', 'test@test.com', 'test', 'test', 'test', 'ROLE_ADMIN', b'0', '2017-10-18 13:25:05', NULL, 0, 0, 1),
(2, 'administrator', 'test1@test1.com', 'test', 'test', 'test', 'ROLE_ADMINISTRATOR', b'0', '2017-10-20 10:21:16', NULL, 0, 0, 1),
(9, 'manager', 't@t.com', 'test', 't1', 't1', 'ROLE_MANAGER', b'0', '2017-10-24 12:21:51', '2017-10-25 08:21:45', 1, 2, 1),
(10, 'user', 'u', 'test', 'u', 'u', 'ROLE_USER', b'0', '2017-10-24 12:22:30', '2017-10-24 14:28:11', 1, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`),
  ADD KEY `FK342arr3oo9hwsqmsm9k5ug1ik` (`administrator`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`),
  ADD KEY `FK75bvwhewa2ts1y1qc85elmm51` (`department_company_id`),
  ADD KEY `FKhk6ge0335bqi76qu2145s9kjj` (`manager`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`document_id`),
  ADD KEY `FK76qkdarb4eehu7ltlocf2jx00` (`owner`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `FKtook7sofqjdu0srtmy2t1yi54` (`user_department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `FK342arr3oo9hwsqmsm9k5ug1ik` FOREIGN KEY (`administrator`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `FK75bvwhewa2ts1y1qc85elmm51` FOREIGN KEY (`department_company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `FKhk6ge0335bqi76qu2145s9kjj` FOREIGN KEY (`manager`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `FK76qkdarb4eehu7ltlocf2jx00` FOREIGN KEY (`owner`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FKtook7sofqjdu0srtmy2t1yi54` FOREIGN KEY (`user_department_id`) REFERENCES `department` (`department_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
